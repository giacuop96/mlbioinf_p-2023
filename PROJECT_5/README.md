# Instruction
   -   Create enviroment by using command : conda env create -f project5.yaml
   -   To generate the preprocessed data and metadata informations, please run Rscript `preprocessing.R` 
   -   Preprocessed data can also be found under `/data/cleanedpreproc_data.csv` and Metadata can be found under  `/data/metadata_col_info.csv` 
